# Node.js Socket.io

This is the old Darkan MMORPG server (from December 2017 till December 2018).
The new game is under development using Pomelo Framework to improve server quality and new art.

This example accompanies the
[Using WebSockets on Heroku with Node.js](https://devcenter.heroku.com/articles/node-websockets)
tutorial.

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)
