var express  = require('express');
var app      = express();
var http     = require('http').Server(app);
var io       = require('socket.io')(http);
var bodyParser = require('body-parser');
var shortId 		= require('shortid');//import shortid lib
var database_model = require('./Models/database_model');
var passive_model = require('./Models/passive_model');
var request = require('request');

app.use(express.static(__dirname));
app.use(bodyParser.urlencoded({ extended: true }));

var itens = {};
var itensName = {};
var clients = {};//storage clients
var clientsOnline = {};
var monsters = [];
var loots = {};
var parties = [];
var clientLookup = {};// cliends search engine
var sockets = {};

var adscount = 0;
var serverexp = 1;
var lotteryGold = 0;

var done = true;

var version = '0.8.8.7v';

var token = "fodasemeupauaidentro";
// Post para saber quem ta online
app.post('/online', function(req, res){	
	var tokenPost = req.body.token;
	if(tokenPost == token){
		res.json(clientsOnline);
		return;
	}else{
		return false;
	}
});

io.engine.ws = new (require('uws').Server)({
    noServer: true,
    clientTracking: false,
    perMessageDeflate: false
});

//open a connection with the database through the file:	./Database/database_model
/*database_model.connect(function (err_connect) {

});*/

//houve uma conexão de algum cliente que abriu alguma aplicação pela primeira vez
io.on('connection', function(socket){

	//console.log(socket.handshake.headers.host);
	socket.setMaxListeners(0);

	io.on('error', (error) => {
	  console.log("meupiru");
	});

	socket.on('error', (error) => {
	  console.log("meupau");
	  console.log(error);
	});
  
    //console.log('A user ready for connection!');
    
    database_model.getMessage(function (err, rows) {
	    if (err) { 
	    	console.error(err); 
	    } else{
	    	//console.log(rows);
	    	res = {
				version: version,
				players: Object.keys(clientsOnline).length,
				adscount: adscount,
				tittleUs: rows[0].tittle,
				messageUs: rows[0].message,
				skipUs: rows[0].skip,
				tittleBr: rows[1].tittle,
				messageBr: rows[1].message,
				skipBr: rows[1].skip
		    };

		    if(socket.handshake.headers.host.includes("192.168.")){
		    	res.skipBr = '1';
		    	res.skipUs = '1';
		    }
		    ////console.log(res);
		    socket.emit("CONNECTED", res);
	    }
 	});

    //emit monsters to player
    socket.emit("MONSTERS", {monsters});
    socket.emit("ITENSLIST", {"itemId":itens, "itemName":itensName});
  	////console.log('connection socket:!'+socket.id);

    var currentUser; //variavel para guardar o cliente corrente que abriu algum dos sockets abaixo
	
	/************************************************************************************************************************************/
	//description: process request of user's registration
	//socket open for the method NetworkController.EmitRegister(string _login , string _pass ) in Unity aplication
	socket.on('REGISTER', function (data){
		//chamada a função assincrona database_model.loadUser que verifica se existe algum usuário com o nome data.name passado como parametro no banco de dados
	   // o resultado e retornado pelas variáveis err ou row e serão processados dentro da callback function (err, rows){processamento...}
    	database_model.creatUser(data.username, function (err, rows) {
    		////console.log(rows);
		    if (rows.length > 0) {
		    	////console.log("ACCOUNT EXIST");
			    result = {
				    exist:"1"//already exits
				};

				socket.emit('REGISTER_RESULT',result);//envia para o metodo NetworkController.OnRegisterResult(SocketIOEvent _result) da aplicacao que abriu este socket
		    }//END-IF
			else
			{
				database_model.verify_charactername(data.charactername, function (err, rows) {
					if (rows.length > 0) {
					    result = {
						    exist:"2"//already exits
						};

						socket.emit('REGISTER_RESULT',result);//envia para o metodo NetworkController.OnRegisterResult(SocketIOEvent _result) da aplicacao que abriu este socket
				    }//END-IF
				    else{
				    	database_model.verify_email(data.email, function (err, rows) {
				    		if (rows.length > 0) {
							    result = {
								    exist:"3"//already exits
								};

								socket.emit('REGISTER_RESULT',result);//envia para o metodo NetworkController.OnRegisterResult(SocketIOEvent _result) da aplicacao que abriu este socket
						    }//END-IF
						    else{
						    	////console.log("trying to create user: "+data.username+" password: "+data.password);
						    	database_model.addUser(data.username, data.charactername, data.email, data.password, function (err, rows) {//TRY ADD USER
								    if (err) { 
								    	console.error(err); 
								    }
								    else {
								        ////console.log("user registered with success! ");
							           result =
							           {
							              exist:"0"

							           };
							           socket.emit('REGISTER_RESULT',result);//envia para o metodo NetworkController.OnRegisterResult(SocketIOEvent _result) da aplicacao que abriu este socket
								    }//END-ELSE
						    	});//END-DATABASE_MODEL.ADDUSER
						    }
				    	});
				    }
				});

				//a callback  function (err, data) {process..} processa o resultado da operação retornado na variavel data
				////console.log("avatar escolhido: "+data.avatar);
				
			}//END-ELSE

	   	});//END-database_model.loadUser
    });//END-SOCET REGISTER

    socket.on('LOGIN', function (data){

    	database_model.verify_user(data.name, data.pass, function(err, rows)
       	{
       		if (err) { 
			 	//console.error(err); 
			 	socket.emit("LOGINFAIL");
			}

       		if (rows[0]) 
       		{
				////console.log("user authenticated!");
				//console.log('[INFO] Player: ' + data.name + ' connected!');
				var user = clientsOnline[data.name];
				if(user != undefined) // found user (still on)
				{
					//socket.broadcast.to(clients[rows[0].id].socketid).emit('HACKING', user);
					delete clients[rows[0].id];
					delete clientsOnline[data.name];

					/*if(!isNaN(user)){ // numero
						delete clientsOnline[data.name];
					} else {
						socket.emit("ALREADYLOGGEDIN");
						return;
					}*/
				}


				helmetArmor = itemArmorById(rows[0].helmet);
				armorArmor = itemArmorById(rows[0].armor);
				necklaceArmor = itemArmorById(rows[0].necklace);
				ringArmor = itemArmorById(rows[0].ring);
				weaponArmor = itemArmorById(rows[0].weapon);
				shieldArmor = itemArmorById(rows[0].shield);
				bootsArmor = itemArmorById(rows[0].boots);
				legsArmor = itemArmorById(rows[0].legs);
				ammoArmor = itemArmorById(rows[0].ammo);

				var totaldefense = helmetArmor + armorArmor + necklaceArmor + ringArmor + bootsArmor + legsArmor + ammoArmor;

				currentUser = {
					socketid: socket.id,
					id: rows[0].id,
					index: rows[0].id,
					name: rows[0].user_name,
					blessed: rows[0].blessed,
					inparty: false,
					charactername: rows[0].character_name,
					avatar: rows[0].avatar,
			        x: rows[0].x,
			        y: rows[0].y,
			        hp: rows[0].hp,
			        maxhp: rows[0].maxhp,
		      		mp: rows[0].mp,
		        	maxmp: rows[0].maxmp,
		        	gold: rows[0].gold,
			        skill: rows[0].skill,
			        pkTime: rows[0].pkTime,
			        direction: "down",
			        lifecharges: rows[0].lifecharges,
			    	maxlifecharges: rows[0].maxlifecharges,
			        head: rows[0].head,
			        skin: rows[0].skin,
			        body: rows[0].body,
			        acessory: rows[0].acessory,
			        lowerbody: rows[0].lowerbody,
			        foots: rows[0].foots,
			        attacksCount: 0,
			        level: rows[0].level,
			        xp: rows[0].xp,
			        points: rows[0].points,
			        melee: rows[0].melee,
			        meleep: rows[0].meleep,
			        magic: rows[0].magic,
			        magicp: rows[0].magicp,
			        distance: rows[0].distance,
			        distancep: rows[0].distancep,
			        slot1: rows[0].slot1,
			        slot2: rows[0].slot2,
			        slot3: rows[0].slot3,
			        slot4: rows[0].slot4,
			        slot5: rows[0].slot5,
			        slot6: rows[0].slot6,
			        slot7: rows[0].slot7,
			        slot8: rows[0].slot8,
			        slot9: rows[0].slot9,
			        helmet: rows[0].helmet,
			        armor: rows[0].armor,
			        necklace: rows[0].necklace,
			        ring: rows[0].ring,
			        weapon: rows[0].weapon,
			        shield: rows[0].shield,
			        boots: rows[0].boots,
			        legs: rows[0].legs,
			        ammo: rows[0].ammo,
			        defense: totaldefense,
				    helmetValue: helmetArmor,
					armorValue: armorArmor,
					necklaceValue: necklaceArmor,
					ringValue: ringArmor,
					weaponType: itens[rows[0].weapon].type,
					weaponValue: weaponArmor,
					shieldValue: shieldArmor,
					bootsValue: bootsArmor,
					legsValue: legsArmor,
					ammoValue: ammoArmor,
					passive: rows[0].passive,
					liferegen: rows[0].liferegen,
					blockchance: rows[0].blockchance,
					evasionpassive: rows[0].evasionpassive,
					lifeblock: rows[0].lifeblock,
					manablock: rows[0].manablock,
					attackcd: rows[0].attackcd,
					manaregen: rows[0].manaregen,
					speed: rows[0].speed,
					hppassive: rows[0].hppassive,
					mppassive: rows[0].mppassive,
					meleepassive: rows[0].meleepassive,
					magicpassive: rows[0].magicpassive,
					distancepassive: rows[0].distancepassive,
					armourpassive: rows[0].armourpassive,
					criticalchancepassive: rows[0].criticalchancepassive,
					lifeattack: rows[0].lifeattack,
					manaattack: rows[0].manaattack,
					damage: 0,
					spelldamage: 0,
					manashield: 0,
					goldToTrade: 0,
					isDead: false
		        };

		        if(currentUser.level <= 100){
		        	currentUser.speed = 1.5 + currentUser.level/125;
		        } else
		        	currentUser.speed = 2.3;
		        
		        currentUser.weapondamage = itemDamageById(rows[0].weapon);
		        

		        clients[rows[0].id] = currentUser;
		        clientsOnline[data.name] = data.name;

		        passive_model.loadpassive(currentUser, rows[0].passive, function (user) {
					//console.log(user);
					currentUser = user;

					currentUser.damage = damageCalculation(currentUser);
		        	currentUser.spelldamage = spellDamageCalculation(currentUser);

			        socket.emit("MYPLAYER", currentUser);
			        // envia para todos os outros players que eu entrei
			        socket.broadcast.emit('OTHERPLAYER', currentUser);
				});
	        }
       	});
    });

    // recebendo a posição de cada player conectado no server
    socket.on('OTHERPLAYERPOS', function(){
    	////console.log('OTHERPLAYERPOS');
		////console.log('currentUser:');
		if(currentUser){
			// console.log(currentUser);
			if(currentUser.pkTime == undefined){
				////console.log("currentUser pkTime undefined");
				currentUser.pkTime = 0;
			}
			socket.broadcast.emit('UPDATEPLAYER', currentUser); // não precisa pedir para cada um mandar suas infos, pode pegar direto do servidor
    	}
    });

    socket.on('UPDATEPLAYERPOS', function(data){
    	if(currentUser){
			////console.log(data);
			// HACKING?
			if((parseInt(data.x) - currentUser.x > 1 || parseInt(data.x) - currentUser.x < -1) || ((parseInt(data.y) - currentUser.y > 1) || parseInt(data.y) - currentUser.y < -1))
				socket.emit("CANNOTMOVE");

			var d = new Date();
			var n = d.getTime();
			var level = currentUser.level;

			if(level > 100)
				level = 100;

			var totalSpeed = (1.5 + (level / 125)) + currentUser.speed;
			////console.log(((n - currentUser.lastMoveAction) / 1000) +' speed: '+totalSpeed);
			// quanto maior a speed, menor o fator. ex: 4 de speed fator = 0.2
			if((n - currentUser.lastMoveAction) < (750/totalSpeed) && !currentUser.charactername.includes('[Darkan]')){ // above 3,3 speed, does enter here
				//console.log("hacking speed? "+currentUser.charactername);
				socket.emit("CANNOTMOVE");
				//socket.emit("HACKING");
				return;
			} else {
			  	currentUser.lastMoveAction = n;
	    		
	    		var x = parseInt(data.x);
	    		var y = parseInt(data.y);

	    		currentUser.x = x;
				currentUser.y = y;
				currentUser.direction = data.dir;

				io.emit('UPDATEPLAYERPOS', currentUser);
	    	}
	    }
    });

    socket.on('UPDATEPLAYERLIFE', function(data){
		if(currentUser){
			if(parseInt(data.life) <= 0){
				console.log(currentUser.charactername+" IS DEAD");

				currentUser.x = 0;
				currentUser.y = 0;
				currentUser.hp = currentUser.maxhp + currentUser.hppassive;
				currentUser.pkTime = 0;
				currentUser.direction = 'down';

				if(!currentUser.blessed){
		    		// xp on death
					currentUser.xp = (currentUser.xp * 99)/100;

					var expToLevel = (50 * (((Math.pow(currentUser.level, 3) - (6 * (Math.pow(currentUser.level, 2)))) + (17 * currentUser.level)) - 12)) / 3;
					if(currentUser.xp < expToLevel)
						currentUser.xp = expToLevel;

					currentUser.meleep = (currentUser.meleep * 80)/100;
					currentUser.magicp = (currentUser.magicp * 80)/100;
					currentUser.distancep = (currentUser.distancep * 80)/100;

					currentUser.gold = (currentUser.gold * 95)/100;
				} else{
					console.log('was blessed');
					currentUser.blessed = false;
				}

				database_model.SavePlayer(currentUser, function (err, rows) {
				    if (err) { 
				    	console.error(err); 
				    }
			 	});
			} else{
				////console.log(currentUser.charactername+' life before: '+currentUser.hp+' after: '+data.life);
				currentUser.hp = parseInt(data.life);
			}
		}	
    });

    socket.on('LIFEREGEN', function(){
    	if(currentUser){

			if(currentUser.hp < (currentUser.maxhp + currentUser.hppassive)){
				currentUser.hp = currentUser.hp + (currentUser.liferegen + (currentUser.level / 2));
				if(currentUser.hp > (currentUser.maxhp + currentUser.hppassive))
					currentUser.hp = currentUser.maxhp + currentUser.hppassive;

				currentUser.hp = Math.floor(currentUser.hp);
				io.emit('UPDATELIFE', currentUser);
			}
		}

    });

    socket.on('REVIVE', function(){
    	if(currentUser){
			io.emit('REVIVE', currentUser);
		}
    });

    socket.on('SKILL', function(data){
    	if(currentUser){

			var d = new Date();
			var n = d.getTime();
			////console.log(((n - currentUser.lastSkillAction) / 1000));
			if(((n - currentUser.lastSkillAction) / 1000) < 3.9){
				//console.log("hacking skill? "+currentUser.charactername);
				//socket.emit("HACKING");
				return;
			}
		  	currentUser.lastSkillAction = n;

			dataSend = {
				charactername: currentUser.charactername,
				level: currentUser.level,
		        skillName: data.skillName,
		        direction: data.dir,
		        targetX: data.x,
		        targetY: data.y,
		        targetName: data.targetName
	        };

	        io.emit("SPAWNSKILL", dataSend);
			//socket.broadcast.emit('SPAWNSKILL', dataSend);
    	}
    });

    socket.on('MONSTERSKILL', function(data){
    	if(currentUser){
			var spellDamage = 1000;

			if(data.monsterName.indexOf('Dragon Lord') !== -1)
				spellDamage = 1500;

			if(data.skillName == "Explosion")
				spellDamage = spellDamage / 2;

			dataSend = {
				monstername: data.monsterName,
		        skillName: data.skillName,
		        direction: data.dir,
		        damage: spellDamage
	        };
	        io.emit("SPAWNMONSTERSKILL", dataSend);
    	}
    });

    socket.on('PK', function(data){
    	if(currentUser){
    		//console.log(data);
    		//console.log(clients);
			var myfriend = false;

			if(currentUser.inparty){
				parties.forEach(function(party){
					if(party.p1 == currentUser.charactername || party.p2 == currentUser.charactername || party.p3 == currentUser.charactername || party.p4 == currentUser.charactername || party.p5 == currentUser.charactername){
						////console.log('found my party, verify if the other player is there');
						if(party.p1 == clients[data.index].charactername || party.p2 == clients[data.index].charactername || party.p3 == clients[data.index].charactername || party.p4 == clients[data.index].charactername || party.p5 == clients[data.index].charactername){
							////console.log('found him');
							myfriend = true;
						}
					}
				});
			}

			if(!myfriend){
				// player não está pk
				//console.log(clients[data.index]);
				if(clients[data.index].pkTime == undefined){
					////console.log('pkTime undefined');
					clients[data.index].pkTime = 0;
				}
				/*else
					//console.log('pkTime not undefined');*/

				//console.log("User "+clients[i].charactername+" is pk: "+clients[i].pkTime);
				if(clients[data.index].pkTime <= 0){
					currentUser.pkTime = 60;
					io.emit("PK", currentUser);
				} else{ // player está pk
					currentUser.yellowSkull = true;
					socket.emit("YELLOWSKULL", currentUser);
					socket.broadcast.to(clients[data.index].socketid).emit('YELLOWSKULL', currentUser);
				}
				currentUser.pkTime = 60;
			}
		}
    });

    socket.on('ATTACK', function (data){
		if(currentUser){
		  	var d = new Date();
			var n = d.getTime();
			var attackcd = currentUser.attackcd;
			if(attackcd < 1.5)
				attackcd = 1.5;
			////console.log(((n - currentUser.lastAction) / 1000) +' atkcd: '+currentUser.attackcd);
			if(((n - currentUser.lastAction) / 1000) < (attackcd - 0.3)){
				//console.log("hacking attack? "+currentUser.charactername);
				//socket.emit("HACKING");
				return;
			}

		  	currentUser.lastAction = n;

		    io.emit('ATTACK', data);//envia para todos os outros clientes a nova posicao do cliente que chamou este socket send to 'UPDATE_MOVE' in networkController in unity app
		    // //console.log(currentUser.name+" Move to "+currentUser.position);
		}
	});

	socket.on('MONSTERATTACK', function (data){
		if(currentUser){
			////console.log('MONSTER DAMAGE: '+data.damage);

			data.damage -= currentUser.defense;

			////console.log('AFTER MONSTER DAMAGE: '+data.damage);

		  	if(currentUser.pkTime > 0){
		  		////console.log(currentUser.charactername+" estava pk e foi atacado, resetar tempo de pk");
		  		currentUser.pkTime = 60;
				io.emit("RESETPKTIME", currentUser);
		  	}
		    io.emit('ATTACK', data);//envia para todos os outros clientes a nova posicao do cliente que chamou este socket send to 'UPDATE_MOVE' in networkController in unity app
		    // //console.log(currentUser.name+" Move to "+currentUser.position);
		}
	});

	socket.on('PING', function (){
		socket.emit("PING");
	});

	// chamado por quem foi atacado
	/*socket.on('VERIFYPK', function (data){
		////console.log("VERIFYPK");
		////console.log(data);
	  	// TODO - melhorar, receber o ID do cliente para tirar o for
  	 	for (var i = 0; i < clients.length; i++)
	 	{
			if (clients[i].charactername == data.name) 
			{
				var myfriend = false;

				if(currentUser.inparty){
					parties.forEach(function(party){
						if(party.p1 == currentUser.charactername || party.p2 == currentUser.charactername || party.p3 == currentUser.charactername || party.p4 == currentUser.charactername || party.p5 == currentUser.charactername){
							////console.log('found my party, verify if the other player is there');
							if(party.p1 == clients[i].charactername || party.p2 == clients[i].charactername || party.p3 == clients[i].charactername || party.p4 == clients[i].charactername || party.p5 == clients[i].charactername){
								////console.log('found him');
								myfriend = true;
							}
						}
					});
				}

				if(!myfriend){
					// player não está pk
					if(clients[i].pkTime == undefined){
						////console.log('pkTime undefined');
						clients[i].pkTime = 0;
					}

					// outro player me atacou e estou pk, outro player fica yellowskull
					if(currentUser.pkTime > 0){
						////console.log(data.name+" me atacou e estou ("+currentUser.charactername+") pk, outro player fica yellowskull");
						if(clients[i].pkTime > 0){
							////console.log(data.name+" tambem esta pk, resetar tempo de pk dele");
							io.emit("RESETPKTIME", clients[i]);
						} else{
				  			socket.emit("YELLOWSKULL", clients[i]);
				  			socket.broadcast.to(clients[i].socketid).emit('YELLOWSKULL', clients[i]);
						}

				  		io.emit("RESETPKTIME", currentUser);
				  	}
				  	else // outro player me atacou e não estou pk
				  	{
						//console.log("User "+clients[i].charactername+" is pk: "+clients[i].pkTime);
						if(clients[i].pkTime <= 0){ // player que me atacou não está pk
							////console.log(data.name+" não está pk, ficar pk");
							clients[i].pkTime = 60;
							io.emit("PK", clients[i]);
						} else{ // player está pk
							////console.log(data.name+" já estava pk, resetar tempo");
							io.emit("RESETPKTIME", clients[i]);
						}
						clients[i].pkTime = 60;
					}
				}
			}
		}
	});*/

	socket.on('PKTIMEOUT', function(data){
		if(currentUser){
			currentUser.pkTime = 0;
			io.emit("PKTIMEOUT", currentUser);
		}
	});

	// função chamada quando um player chega na posição desejada
	socket.on('CURRENTPOS', function(data){
		if(currentUser){
			currentUser.x = parseInt(data.x);
			currentUser.y = parseInt(data.y);
			io.emit("CURRENTPOS", currentUser);
		}
	});

	// quando um player entra na visão de um monstro
	socket.on('MONSTERTARGET', function(data){
		monsters[data.index].target = data.playerName;
		io.emit("MONSTERTARGET", monsters[data.index]);
	});

	socket.on('SETMONSTERPOSITION', function(data){
		if(currentUser){
			monsters[data.index].x = parseInt(data.x);
			monsters[data.index].y = parseInt(data.y);
		}
	});

	socket.on('MONSTERMOVEPOSITION', function(data){
		var monsterpos = monsters.find(key => (key.currentX == data.x && key.currentY == data.y));
		var userpos = Object.keys(clients).find(key => (clients[key.x] == data.x && clients[key.y] == data.y));

		if(monsterpos == undefined && userpos == undefined){
    		monsters[data.index].x = parseInt(data.x);
			monsters[data.index].y = parseInt(data.y);

			socket.broadcast.emit("MONSTERMOVEPOSITION", monsters[data.index]);
		} 
	});

	socket.on('MONSTERLIFE', function(data){
		if(currentUser){
			// calculate player damage, decrease mob hp

			//console.log('dano: '+currentUser.damage);
			if(data.type == "attack")
				data.damage = currentUser.damage - monsters[data.index].defense;
			else if(data.type == "spell")
				data.damage = currentUser.spelldamage - monsters[data.index].defense;

			if(data.damage > 0){
				//console.log('dano apos penetrar defesa: '+data.damage);

				data.criticized = false;
			  	var damageVariation = Math.floor((Math.random() * 20) + 1);
				var plusOrMinus = Math.random() < 0.5 ? -1 : 1;
				data.damage = parseInt(data.damage) + (damageVariation * plusOrMinus * (data.damage / 100));

				//console.log('dano apos variacao: '+data.damage);

				var critical = Math.random() < (currentUser.criticalchancepassive/100) ? -1 : 1;

				if(critical == -1){
					data.criticized = true;
					data.damage *= 1.5;
					//console.log('dano apos critico: '+data.damage);
				}

				data.damage = Math.round(data.damage);
				
				//console.log('ATTACK contando ataques para ganhar skill');
				if(currentUser.weaponType == 'melee'){
					currentUser.meleep++;

					var nextSkill = currentUser.melee + 1;
					var attacksToSkll = 50 * Math.pow(1.1, (currentUser.melee - 10));

					////console.log('current skill: '+currentUser.melee);
					////console.log('attacks count: '+currentUser.meleep);
					////console.log('total attacks at: '+attacksToSkll);

					if(currentUser.meleep >= attacksToSkll){
						////console.log('advanced skill to '+nextSkill);
						currentUser.melee = nextSkill;
						currentUser.meleep = 0;
					}
				} else if(currentUser.weaponType == 'magic'){
					currentUser.magicp++;

					var nextSkill = currentUser.magic + 1;
					var attacksToSkll = 50 * Math.pow(1.1, (currentUser.magic - 10));

					//console.log('current skill: '+currentUser.magic);
					//console.log('attacks count: '+currentUser.magicp);
					//console.log('total attacks at: '+attacksToSkll);

					if(currentUser.magicp >= attacksToSkll){
						////console.log('advanced skill to '+nextSkill);
						currentUser.magic = nextSkill;
						currentUser.magicp = 0;
					}
				} else if(currentUser.weaponType == 'distance'){
					currentUser.distancep++;

					var nextSkill = currentUser.distance + 1;
					var attacksToSkll = 50 * Math.pow(1.1, (currentUser.distance - 10));

					////console.log('current skill: '+currentUser.distance);
					////console.log('attacks count: '+currentUser.distancep);
					////console.log('total attacks at: '+attacksToSkll);

					if(currentUser.distancep >= attacksToSkll){
						////console.log('advanced skill to '+nextSkill);
						currentUser.distance = nextSkill;
						currentUser.distancep = 0;
					}
				}

				////console.log('HP: '+currentUser.hp);
			  	if(currentUser.lifeattack > 0 && currentUser.hp < (currentUser.maxhp + currentUser.hppassive)){
			  		currentUser.hp = parseInt(currentUser.hp) + currentUser.lifeattack;
			  		////console.log('HP AFTER: '+currentUser.hp);
			  		if(currentUser.hp > (currentUser.maxhp + currentUser.hppassive))
			  			currentUser.hp = currentUser.maxhp + currentUser.hppassive;

			  		io.emit('ATTACKHEAL', currentUser);
			  	}

			  	monsters[data.index].hp -= data.damage;
			}

			//console.log(currentUser.maxlifecharges + Math.floor(currentUser.level / 10));
		  	if(currentUser.lifecharges < ((currentUser.maxlifecharges + 2) + Math.floor(currentUser.level / 2))) {
		  		currentUser.attacksCount++;
		  		if(currentUser.attacksCount >= 15){ // TODO - substituir pela quantidade de ataques necessario
		  			currentUser.attacksCount = 0;
		  			currentUser.lifecharges++;
		  			////console.log('GAINLIFEPOTION');
		  			////console.log(currentUser);
		  			socket.emit("GAINLIFEPOTION");
		  		}
		  	}

		  	if(currentUser.pkTime > 0){
		  		////console.log(currentUser.charactername+" estava pk e atacou, resetar tempo de pk");
		  		currentUser.pkTime = 60;
	    		io.emit("RESETPKTIME", currentUser);
		  	}

			data.name = monsters[data.index].name;
			data.hp = monsters[data.index].hp;
			data.maxhp = monsters[data.index].maxhp;


			io.emit('MONSTERLIFE', data);
			if(data.hp <= 0)
			{
				var monster = monsters[data.index];
				if(!monster.isDead){
					monster.isDead = true;
					io.emit('MONSTERDEAD', monster);

					if(monster.respawn != 0)
						setTimeout(myFunc, monster.respawn, data.index);

					// calculating the loot
					/*//console.log(monsters[i].loot-1);
					//console.log(loots[monsters[i].loot-1]);
					//console.log(loots[monsters[i].loot-1]['chance1']);*/

					goldPercent = Math.floor((Math.random() * 10) + 1);

					goldVariation = loots[monster.loot].gold * (goldPercent / 100);
					if(goldVariation < 1)
						goldVariation = 1;

					var plusOrMinus = Math.random() < 0.5 ? -1 : 1;

					gold = loots[monster.loot].gold + goldVariation * plusOrMinus;

					var drop = 0;
					var drop1 = 0;
					var drop2 = 0;
					var drop3 = 0;
					var drop4 = 0;
					var drop5 = 0;

					var drop1name = '';
					var drop2name = '';
					var drop3name = '';
					var drop4name = '';
					var drop5name = '';

					dropChance = Math.floor(Math.random() * 5000);

					if(dropChance < loots[monster.loot].chance1){
						drop1 = loots[monster.loot].item1id;
						drop1name = itens[drop1].name;
					}

					dropChance = Math.floor(Math.random() * 5000);
					//console.log('drop2: '+dropChance+' chance: '+loots[monsters[i].loot-1]['chance2']);
					
					if(dropChance < loots[monster.loot].chance2){
						drop2 = loots[monster.loot].item2id;
						drop2name = itens[drop2].name;
					}

					dropChance = Math.floor(Math.random() * 5000);
					////console.log('drop3: '+dropChance+' chance: '+loots[monsters[i].loot-1]['chance3']);
					
					if(dropChance < loots[monster.loot].chance3){
						drop3 = loots[monster.loot].item3id;
						drop3name = itens[drop3].name;
					}

					dropChance = Math.floor(Math.random() * 5000);
					////console.log('drop4: '+dropChance+' chance: '+loots[monsters[i].loot-1]['chance4']);
					
					if(dropChance < loots[monster.loot].chance4){
						drop4 = loots[monster.loot].item4id;
						drop4name = itens[drop4].name;
					}

					dropChance = Math.floor(Math.random() * 5000);
					////console.log('drop5: '+dropChance+' chance: '+loots[monsters[i].loot-1]['chance5']);
					
					if(dropChance < loots[monster.loot].chance5){
						drop5 = loots[monster.loot].item5id;
						drop5name = itens[drop5].name;
					}

					if(drop1 != 0 || drop2 != 0 || drop3 != 0 || drop4 != 0 || drop5 != 0){
						drop = 1;

						dropData = {
							posx: monster.x,
							posy: monster.y,
							item1: drop1,
							item2: drop2,
							item3: drop3,
							item4: drop4,
							item5: drop5,
							item1name: drop1name,
							item2name: drop2name,	
							item3name: drop3name,
							item4name: drop4name,
							item5name: drop5name
						}

						////console.log('DROP');
						//console.log(dropData);
					}

					if(currentUser.inparty){
						var party = parties[currentUser.party];
						if(party == undefined){
							console.log("ERRO, undefined party");
							console.log(currentUser);
							console.log(parties);
							return;
						}

						var partyPlayersCount = 1;
						var currentIndex = party.p1id;

						var partycount = 0.8;
						if(party.p2 != ''){
							partycount-=0.1;
							partyPlayersCount++;
						}
						if(party.p3 != ''){
							partycount-=0.1;
							partyPlayersCount++;
						}
						if(party.p4 != ''){
							partycount-=0.1;
							partyPlayersCount++;
						}
						if(party.p5 != ''){
							partycount-=0.1;
							partyPlayersCount++;
						}

						for (var j = 1; j <= partyPlayersCount; j++)
					 	{
					 		currentIndex = party['p'+j+'id'];

					 		if(currentIndex != ''){
					 			if(clients[currentIndex] != undefined){
							 		if(clients[currentIndex].inparty){

								 		var canreceiveexp = false;
								 		if(clients[currentIndex].x - monster.x > -10 && clients[currentIndex].x - monster.x < 10){
											if(clients[currentIndex].y - monster.y > -10 && clients[currentIndex].y - monster.y < 10){
												canreceiveexp = true;
											}
										}

										if(canreceiveexp){
											////console.log('gold before: '+clients[j].gold);
											clients[currentIndex].gold += Math.round(gold * partycount);
											////console.log('gold after: '+clients[j].gold);
											var mobexp = monster.xp;
											if(clients[currentIndex].level > monster.level){
												mobexp = (1 - ((clients[currentIndex].level - monster.level) / 100)) * mobexp;
											}

											clients[currentIndex].xp = parseInt(clients[currentIndex].xp) + Math.round(mobexp * partycount * serverexp);
										  	var data = {
												charactername: clients[currentIndex].charactername,
												xpgained: Math.round(mobexp * partycount * serverexp),
												gold: Math.round(clients[currentIndex].gold)
											}
											socket.broadcast.to(clients[currentIndex].socketid).emit('GAINXP', data);

											if(drop){
												var rand = Math.floor(Math.random() * 10);
												if(rand <= 3){
													//console.log('drop to: '+clients[currentIndex].charactername);
													drop = 0;
													// TODO - enviar para todos (todos devem ver a bag)
													//console.log('drop to: '+clients[j].charactername);
													if(clients[currentIndex].charactername == currentUser.charactername)
														socket.emit('MONSTERDROP', dropData);
													else
														socket.broadcast.to(clients[currentIndex].socketid).emit('MONSTERDROP', dropData);
												}
											}

											// level up
											var nextLevel = clients[currentIndex].level + 1;
											var expToLevel = (50 * (((Math.pow(nextLevel, 3) - (6 * (Math.pow(nextLevel, 2)))) + (17 * nextLevel)) - 12)) / 3;

											if(expToLevel - parseInt(clients[currentIndex].xp) <= 0){
												clients[currentIndex].level = nextLevel;
												if(nextLevel <= 100)
													clients[currentIndex].points = clients[currentIndex].points + 1;
												clients[currentIndex].maxhp += 20;
												clients[currentIndex].maxmp += 20;
												io.emit("PLAYERLEVELUP", clients[currentIndex]);
											}
										}
									} else{
										console.log("ERRO, clients[currentIndex] not in party");
										console.log(clients[currentIndex]);
									}
								} else{
									console.log("ERRO, clients[currentIndex] undefined");
									console.log(clients[currentIndex]);
								}
							}
					 	}

						var dataExtra = {
							charactername: currentUser.charactername,
							xpgained: Math.round(monster.xp * partycount * serverexp),
							gold: Math.round(currentUser.gold)
						}
						socket.emit("GAINXP", dataExtra);

						if(drop){
							// TODO - enviar para todos (todos devem ver a bag)
							socket.emit('MONSTERDROP', dropData);
						}
					} else{ // not in party
						// add gold to player
						currentUser.gold += gold;

						// add xp to player
						var mobexp = monster.xp;
						if(currentUser.level > monster.level){
							mobexp = (1 - ((currentUser.level - monster.level) / 100)) * mobexp;
							//console.log(mobexp);
						}

						currentUser.xp = parseInt(parseInt(currentUser.xp) + (mobexp * serverexp));
						var data = {
							charactername: currentUser.charactername,
							xpgained: parseInt(mobexp * serverexp),
							gold: Math.round(currentUser.gold)
						}
						socket.emit("GAINXP", data);

						if(drop){
							// TODO - enviar para todos (todos devem ver a bag)
							socket.emit("MONSTERDROP", dropData);
						}
					}

					var nextLevel = currentUser.level + 1;
					var expToLevel = (50 * (((Math.pow(nextLevel, 3) - (6 * (Math.pow(nextLevel, 2)))) + (17 * nextLevel)) - 12)) / 3;
					////console.log('current exp: '+currentUser.xp);
					////console.log('exp at level '+nextLevel+' : '+expToLevel);
					////console.log('exp to up: '+(expToLevel - parseInt(currentUser.xp)));

					if(expToLevel - parseInt(currentUser.xp) <= 0){ //advanced level
						////console.log(currentUser.name+' advanced from level '+currentUser.level+' to level '+nextLevel);
						currentUser.level = nextLevel;
						if(nextLevel <= 100)
							currentUser.points = currentUser.points + 1;
						currentUser.maxhp += 20;
						currentUser.maxmp += 20;
						io.emit("PLAYERLEVELUP", currentUser);
					}
		    	}
			}
		}
	});

	socket.on('MONSTERLOSETARGET', function(data){
		monsters[data.index].target = null;
		socket.broadcast.emit('MONSTERLOSETARGET', monsters[data.index]);
	});

	socket.on('PLAYERCOLOR', function(data){
		if(currentUser){
			////console.log(data);
			if(data.head != 'RGBA(0.000, 0.000, 0.000, 0.000)')
				currentUser.head = data.head.replace(/ /g, '');
			if(data.skin != 'RGBA(0.000, 0.000, 0.000, 0.000)')
				currentUser.skin = data.skin.replace(/ /g, '');
			if(data.body != 'RGBA(0.000, 0.000, 0.000, 0.000)')
				currentUser.body = data.body.replace(/ /g, '');
			if(data.acessory != 'RGBA(0.000, 0.000, 0.000, 0.000)')
				currentUser.acessory = data.acessory.replace(/ /g, '');
			if(data.legs != 'RGBA(0.000, 0.000, 0.000, 0.000)')
				currentUser.lowerbody = data.legs.replace(/ /g, '');
			if(data.boots != 'RGBA(0.000, 0.000, 0.000, 0.000)')
				currentUser.foots = data.boots.replace(/ /g, '');

			database_model.SavePlayerColor(currentUser, function (err, rows) {
			    if (err) { 
			    	console.error(err); 
			    }
			    else{
			    	////console.log('color updated');
			    	socket.broadcast.emit("PLAYERCOLOR", currentUser);
			    }
		 	});
		}
	});

	//OTIMIZAR/AJEITAR BUG DA POTION N ENCHER QUANDO LEVA HIT
	socket.on('PLAYERLIFEPOTION', function(){
		if(currentUser){
			var d = new Date();
			var n = d.getTime();
			////console.log(((n - currentUser.lastPotionAction) / 1000));
			if(((n - currentUser.lastPotionAction) / 1000) < 1){
				////console.log("hacking potion?");
				//socket.emit("HACKING");
				return;
			}
		  	currentUser.lastPotionAction = n;

			if(parseInt(currentUser.lifecharges) > 0){
				currentUser.lifecharges = parseInt(currentUser.lifecharges) - 1;

				//var toHeal = currentUser.hp;
				var oldlife = currentUser.hp;
				currentUser.hp = currentUser.hp + (0.15 * (currentUser.maxhp + currentUser.hppassive));
				if(currentUser.hp > (currentUser.maxhp + currentUser.hppassive))
					currentUser.hp = currentUser.maxhp + currentUser.hppassive;

				////console.log(currentUser.charactername+' life: '+oldlife+' to: '+currentUser.hp);

				io.emit('PLAYERLIFEPOTION', currentUser);
			} else{
				//console.log('desync? player tried to use life potion without charges');
			}
		}
	});

	socket.on('PLAYERCHARACTER', function(){
		if(currentUser){
			currentUser.xp = parseInt(currentUser.xp);
			currentUser.meleep = parseInt(currentUser.meleep);
			currentUser.magicp = parseInt(currentUser.magicp);
			currentUser.distancep = parseInt(currentUser.distancep);
			
			socket.emit("PLAYERCHARACTER", currentUser);
		}
	});

	socket.on('PLAYERINVENTORY', function(){
		if(currentUser){
			currentUser.xp = parseInt(currentUser.xp);
			currentUser.meleep = parseInt(currentUser.meleep);
			currentUser.magicp = parseInt(currentUser.magicp);
			currentUser.distancep = parseInt(currentUser.distancep);

			socket.emit("PLAYERINVENTORY", currentUser);
		}
	});

	socket.on('PLAYERINVENTORYONTRADE', function(data){
		if(currentUser){
			currentUser.slot1 = parseInt(data.slot1);
			currentUser.slot2 = parseInt(data.slot2);
			currentUser.slot3 = parseInt(data.slot3);
			currentUser.slot4 = parseInt(data.slot4);
			currentUser.slot5 = parseInt(data.slot5);
			currentUser.slot6 = parseInt(data.slot6);
			currentUser.slot7 = parseInt(data.slot7);
			currentUser.slot8 = parseInt(data.slot8);
			currentUser.slot9 = parseInt(data.slot9);

			socket.emit("PLAYERINVENTORYONTRADE", currentUser);
		}
	});

	// OTIMIZAR
	socket.on('MESSAGE', function(data){
		if(currentUser && data.message != undefined){
			//console.log(data);
			if(data.message.includes('/') && currentUser.charactername.includes('[Darkan]')){ // cmd de gm
				var cmd = data.message.split(" ");
				if(data.message.includes('save'))
					saveFunc();
				else if(data.message.includes('lg')){ // setting lottery gold
					lotteryGold = parseInt(cmd[1]);
				}
				else if(data.message.includes('ad')){
					adscount = parseInt(cmd[1]);
					if(adscount >= 100){
						serverexp = adscount / 100;
						adscount = 0;
						setTimeout(expFunc, 3600000, 1); //3600000 // 1 hour
					}
					io.emit('ADSCOUNT', {count:adscount});
				} else if(data.message.includes('monster')){
					var monsterName = cmd[1].replace('_', ' ');
					database_model.get_monster(monsterName, function (err, rows) {
						if (err) { 
							console.error(err); 
						}
						else{
							var monster = rows[0];
							monster.respawn = 0;
					    	monster.target = null;
					    	monster.isDead = false;
					    	monster.name = monster.name;
					    	monster.startX = parseInt(cmd[2]);
					    	monster.startY = parseInt(cmd[3]);
					    	monster.currentX = parseInt(cmd[2]);
					    	monster.currentY = parseInt(cmd[3]);
					    	monster.x = parseInt(cmd[2]);
					    	monster.y = parseInt(cmd[3]);
					    	monster.loot = monster.loot;
					    	monsters.push(monster);
						    io.emit("MONSTER", monster);
						}
					});
				} else if(cmd[0] == '/teleport'){
					var playername = cmd[1].replace('_', ' ');
					for (var i = 0; i < clients.length; i++)
				 	{
						if (clients[i].charactername == playername) 
						{
							clients[i].x = parseInt(cmd[2]);
							clients[i].y = parseInt(cmd[3]);
							io.emit('PLAYERTELEPORTTO', clients[i]);
							break;
						}
					}
				} else if(data.message.includes('teleportall')){
					for (var i = 0; i < clients.length; i++)
				 	{
						clients[i].x = parseInt(cmd[1]);
						clients[i].y = parseInt(cmd[2]);
						io.emit('PLAYERTELEPORTTO', clients[i]);
					}
				}
			} else
				io.emit("MESSAGE", data);
		}
	});

	socket.on('ITEMDESTROY', function(data){
		if(currentUser){
			data.slot = parseInt(data.slot) + 1;

			if(data.slot == 1)
				currentUser.slot1 = 0;
			else if(data.slot == 2)
				currentUser.slot2 = 0;
			else if(data.slot == 3)
				currentUser.slot3 = 0;
			else if(data.slot == 4)
				currentUser.slot4 = 0;
			else if(data.slot == 5)
				currentUser.slot5 = 0;
			else if(data.slot == 6)
				currentUser.slot6 = 0;
			else if(data.slot == 7)
				currentUser.slot7 = 0;
			else if(data.slot == 8)
				currentUser.slot8 = 0;
			else if(data.slot == 9)
				currentUser.slot9 = 0;

			/*database_model.destroyItem(currentUser.charactername, data.slot, function (err, rows) {
				//console.log(rows);
				//socket.emit('ITEMINFO', rows[0]);
			});*/
		}
	});

	socket.on('RANK', function(data){
		levelrank = {
			name1: '',
			level1: '',
			name2: '',
			level2: '',
			name3: '',
			level3: '',
			name4: '',
			level4: '',
			name5: '',
			level5: '',
			name6: '',
			level6: '',
			name7: '',
			level7: '',
			name8: '',
			level8: '',
			name9: '',
			level9: '',
			name10: '',
			level10: '',
		};

		i = 1;
		j = 1;
		k = 1;
		l = 1;

			////console.log("SHOW RANK");

		database_model.get_levelrank(function (err, rows) {
			rows.forEach(function(player){
				levelrank["name"+i] = player.character_name;
   				levelrank["level"+i] = player.level;

				i++;
		  	});

 			////console.log(levelrank);
 			socket.emit('LEVELRANK', levelrank);
 		});

 		database_model.get_meleerank(function (err, rows) {
			rows.forEach(function(player){
				levelrank["name"+j] = player.character_name;
   				levelrank["level"+j] = player.melee;

				j++;
		  	});

 			////console.log(levelrank);
 			socket.emit('MELEERANK', levelrank);
 		});

 		database_model.get_magicrank(function (err, rows) {
			rows.forEach(function(player){
				levelrank["name"+k] = player.character_name;
   				levelrank["level"+k] = player.magic;

				k++;
		  	});

 			////console.log(levelrank);
 			socket.emit('MAGICRANK', levelrank);
 		});

 		database_model.get_distancerank(function (err, rows) {
			rows.forEach(function(player){
				levelrank["name"+l] = player.character_name;
   				levelrank["level"+l] = player.distance;

				l++;
		  	});

 			////console.log(levelrank);
 			socket.emit('DISTANCERANK', levelrank);
 		});
	});

	socket.on('UPDATEMANA', function(data){
		if(currentUser)
			currentUser.mp = data.mp;
	});

	socket.on('PLAYERSPELL', function(data){
		if(currentUser){
			currentUser.skill = data.spell;
			currentUser.spelldamage = spellDamageCalculation(currentUser);
		}
	});

	socket.on('SANTACLAUSROLL', function(data){
		if(currentUser){
			if(data.won == 1){
				currentUser.gold = parseInt(currentUser.gold) + parseInt(data.gold);
				////console.log(currentUser.charactername+" won "+data.gold+" at santa. gold now = "+currentUser.gold);
			} else{
				currentUser.gold = parseInt(currentUser.gold) - parseInt(data.gold);
				////console.log(currentUser.charactername+" lost "+data.gold+" at santa. gold now = "+currentUser.gold);
			}
		}
	});

	socket.on('GOLD', function(data){
		if(currentUser)
			currentUser.gold = parseInt(data.gold);
	});

	// OTIMIZAR
	socket.on('BOUGHTITEM', function(data){
		if(currentUser){

			if(data.slot == '-1'){
				////console.log('no free slot');
				return;
			}

			data.slot = parseInt(data.slot) + 1;
			////console.log('slot: '+data.slot+' itemName: '+data.itemName);

			if(data.slot == 1)
				currentUser.slot1 = data.id;
			else if(data.slot == 2)
				currentUser.slot2 = data.id;
			else if(data.slot == 3)
				currentUser.slot3 = data.id;
			else if(data.slot == 4)
				currentUser.slot4 = data.id;
			else if(data.slot == 5)
				currentUser.slot5 = data.id;
			else if(data.slot == 6)
				currentUser.slot6 = data.id;
			else if(data.slot == 7)
				currentUser.slot7 = data.id;
			else if(data.slot == 8)
				currentUser.slot8 = data.id;
			else if(data.slot == 9)
				currentUser.slot9 = data.id;

			currentUser.gold = parseInt(currentUser.gold) - parseInt(data.value);

			////console.log(currentUser);

			/*database_model.addItem(currentUser.charactername, data.itemName, data.slot, function (err, rows) {
					//console.log(rows);
	 		});*/
 		}
	});

	// OTIMIZAR
	socket.on('EQUIPITEM', function(data){
		if(currentUser){
			if(data.itemType != "melee" && data.itemType != "magic" && data.itemType != "distance" && data.itemType != "shield")
				currentUser.defense += itemArmorById(data.id);

			data.inventorySlot = parseInt(data.inventorySlot) + 1;
			////console.log('slot: '+data.inventorySlot);
			if(data.inventorySlot == '1'){
				currentUser.slot1 = 0;
			} else if(data.inventorySlot == '2'){
				currentUser.slot2 = 0;
			} else if(data.inventorySlot == '3'){
				currentUser.slot3 = 0;
			} else if(data.inventorySlot == '4'){
				currentUser.slot4 = 0;
			} else if(data.inventorySlot == '5'){
				currentUser.slot5 = 0;
			} else if(data.inventorySlot == '6'){
				currentUser.slot6 = 0;
			} else if(data.inventorySlot == '7'){
				currentUser.slot7 = 0;
			} else if(data.inventorySlot == '8'){
				currentUser.slot8 = 0;
			} else if(data.inventorySlot == '9'){
				currentUser.slot9 = 0;
			}

			var oldItem = 0;
			var oldItemType = "equip";
			////console.log('type: '+data.itemType);

			if(data.itemType == "armor"){
				if(currentUser.armor != 0)
					oldItem = currentUser.armor;
				currentUser.armor = data.id;
			} else if(data.itemType == "helmet"){
				if(currentUser.helmet != 0)
					oldItem = currentUser.helmet;
				currentUser.helmet = data.id;
			} else if(data.itemType == "necklace"){
				if(currentUser.necklace != 0)
					oldItem = currentUser.necklace;
				currentUser.necklace = data.id;
			} else if(data.itemType == "legs"){
				if(currentUser.legs != 0)
					oldItem = currentUser.legs;
				currentUser.legs = data.id;
			} else if(data.itemType == "boots"){
				if(currentUser.boots != 0)
					oldItem = currentUser.boots;
				currentUser.boots = data.id;
			} else if(data.itemType == "ring"){
				if(currentUser.ring != 0)
					oldItem = currentUser.ring;
				currentUser.ring = data.id;
			} else if(data.itemType == "shield"){
				if(currentUser.shield != 0)
					oldItem = currentUser.shield;
				currentUser.shield = data.id;
			} else if(data.itemType == "ammo"){
				if(currentUser.ammo != 0)
					oldItem = currentUser.ammo;
				currentUser.ammo = data.id;
			} else if(data.itemType == "melee" || data.itemType == "magic" || data.itemType == "distance"){
				if(currentUser.weapon != 0){
					oldItem = currentUser.weapon;
					oldItemType = "weapon";
				}

				currentUser.weapon = data.id;
				currentUser.weaponType = data.itemType;

				currentUser.damage = damageCalculation(currentUser);
			}

			if(oldItem != 0 && oldItemType != "weapon") // tirando defesa do item anterior
				currentUser.defense -= itemArmorById(oldItem)

			if(currentUser.slot1 == 0){
				currentUser.slot1 = oldItem;
			} else if(currentUser.slot2 == 0){
				currentUser.slot2 = oldItem;
			} else if(currentUser.slot3 == 0){
				currentUser.slot3 = oldItem;
			} else if(currentUser.slot4 == 0){
				currentUser.slot4 = oldItem;
			} else if(currentUser.slot5 == 0){
				currentUser.slot5 = oldItem;
			} else if(currentUser.slot6 == 0){
				currentUser.slot6 = oldItem;
			} else if(currentUser.slot7 == 0){
				currentUser.slot7 = oldItem;
			} else if(currentUser.slot8 == 0){
				currentUser.slot8 = oldItem;
			} else if(currentUser.slot9 == 0){
				currentUser.slot9 = oldItem;
			}
		}
	});

	// OTIMIZAR
	socket.on('UNEQUIPTEM', function(data){
		if(currentUser){
			//console.log('--------------- BEFORE --------------');
			//console.log(currentUser);
			if(data.itemType != "weapon" && data.itemType != "shield")
				currentUser.defense -= itemArmorById(data.id);

			data.inventorySlot = parseInt(data.inventorySlot) + 1;

			//console.log('slot: '+data.inventorySlot);

			if(data.inventorySlot == '1'){
				currentUser.slot1 = data.id;
			} else if(data.inventorySlot == '2'){
				currentUser.slot2 = data.id;
			} else if(data.inventorySlot == '3'){
				currentUser.slot3 = data.id;
			} else if(data.inventorySlot == '4'){
				currentUser.slot4 = data.id;
			} else if(data.inventorySlot == '5'){
				currentUser.slot5 = data.id;
			} else if(data.inventorySlot == '6'){
				currentUser.slot6 = data.id;
			} else if(data.inventorySlot == '7'){
				currentUser.slot7 = data.id;
			} else if(data.inventorySlot == '8'){
				currentUser.slot8 = data.id;
			} else if(data.inventorySlot == '9'){
				currentUser.slot9 = data.id;
			}

			//console.log('type: '+data.itemType);

			if(data.itemType == "armor"){
				currentUser.armor = 0;
			} else if(data.itemType == "helmet"){
				currentUser.helmet = 0;
			} else if(data.itemType == "necklace"){
				currentUser.necklace = 0;
			} else if(data.itemType == "legs"){
				currentUser.legs = 0;
			} else if(data.itemType == "boots"){
				currentUser.boots = 0;
			} else if(data.itemType == "ring"){
				currentUser.ring = 0;
			} else if(data.itemType == "shield"){
				currentUser.shield = 0;
			} else if(data.itemType == "ammo"){
				currentUser.ammo = 0;
			} else if(data.itemType == "melee" || data.itemType == "magic" || data.itemType == "distance" || data.itemType == "weapon"){
				currentUser.weapon = 0;
				currentUser.damage = currentUser.level;
			}

			//console.log('--------------- AFTER --------------');
			//console.log(currentUser);

			////console.log(currentUser);
			socket.emit('UNEQUIPTEM');
		}
	});

	// OTIMIZAR
	socket.on('PASSIVE', function(data){
		if(currentUser){
			if(parseInt(currentUser.points) > 0){
				////console.log('NEW PASSIVE: '+data.passive);

				var point = data.passive.replace(currentUser.passive, '');
				////console.log('point: '+point+' lgt: '+point.length);

				if(point.length <= 4){
					if(currentUser.passive.indexOf('-'+point) !== -1){
						//console.log('already has this point!');
					} else {
						////console.log('new point!');
						currentUser.passive = data.passive;
						currentUser.points = parseInt(currentUser.points) - 1;
					}
				}
			}
		}
	});

	// OTIMIZAR
	socket.on('POINTSREQUEST', function(data){
		if(currentUser){
			if(parseInt(currentUser.points) > 0){
				socket.emit('POINTSREQUEST');
			}
		}
	});

	// OTIMIZAR
	socket.on('UPDATEPLAYERSTATUS', function(data){
		if(currentUser){
			currentUser.maxlifecharges = parseInt(data.maxLifeCharges);
			currentUser.liferegen = parseInt(data.lifeRegen);
			currentUser.blockchance = parseInt(data.blockChance);
			currentUser.evasionpassive = parseInt(data.evasionChance);
			currentUser.lifeblock = parseInt(data.lifeBlock);
			currentUser.manablock = parseInt(data.manaBlock);
			currentUser.attackcd = parseFloat(data.attackCD);
			currentUser.manaregen = parseInt(data.manaRegen);
			currentUser.speed = parseFloat(data.speed);

			currentUser.meleepassive = parseInt(data.meleePassive);
			currentUser.magicpassive = parseInt(data.magicPassive);
			currentUser.distancepassive = parseInt(data.distancePassive);

			currentUser.armourpassive = parseInt(data.armourPassive);
			currentUser.criticalchancepassive = parseInt(data.criticalChancePassive);
			currentUser.lifeattack = parseInt(data.lifeAttack);
			currentUser.manaattack = parseInt(data.manaAttack);

			currentUser.hppassive = parseInt(data.hpPassive);
			currentUser.mppassive = parseInt(data.mpPassive);

			currentUser.damage = damageCalculation(currentUser);
			currentUser.spelldamage = spellDamageCalculation(currentUser);
		}
	});

	socket.on('PASSIVESTATS', function(data){
		if(currentUser){
			var attackspeedS = ((3 - currentUser.attackcd) / 0.015).toFixed(2);
			if (attackspeedS < 0) {
				attackspeedS = 0;
				attackspeedS = "<color=red>" + attackspeedS + "</color>";
			} else if (attackspeedS > 0) {
				if(attackspeedS >= 100)
					attackspeedS = 100;
				attackspeedS = '<color=green>+' + attackspeedS + "</color>";
			} else {
				attackspeedS += "0";
			}

			var manaS = currentUser.maxmp;
			var mppassive = currentUser.mppassive
			if((currentUser.maxmp + currentUser.mppassive) < 0){
				manaS = 0;
				mppassive = 0;
			}

			var criticalchanceS = currentUser.criticalchancepassive;
			if(criticalchanceS <= 0){
				criticalchanceS = 0;
			} else{
				criticalchanceS = '<color=green>+' + criticalchanceS + "</color>";
			}

			var evasionchanceS = currentUser.evasionpassive;
			if(evasionchanceS <= 0){
				evasionchanceS = 0;
			} else{
				evasionchanceS = '<color=green>+' + evasionchanceS + "</color>";
			}

			var blockchanceS = currentUser.blockchance;
			if(blockchanceS <= 0){
				blockchanceS = 0;
			} else{
				blockchanceS = '<color=green>+' + blockchanceS + "</color>";
			}

			//montar texto da passiva do player aqui
			var col1 = "Bleesed: "+currentUser.blessed+"\n";
			col1 = "X: "+currentUser.x+" Y: "+currentUser.y+"\n";
			col1 = "Damage: "+currentUser.damage.toFixed(2)+"\n";
			col1 += "Defense: "+currentUser.defense+"\n";
			col1 += "Increased Armor: "+currentUser.armourpassive+"%\n";
			col1 += "Speed: +"+currentUser.speed.toFixed(2)+"\n";
			col1 += "Block Chance: +"+blockchanceS+"%\n";
			col1 += "Evasion Chance: +"+evasionchanceS+"%\n\n";

			var col2 = "Attack Speed: "+attackspeedS+"%\n";
			col2 += "Critical Chance: "+criticalchanceS+"%\n";
			col2 += "Physical Damage: +"+currentUser.meleepassive+"%\n";
			col2 += "Magic Damage: +"+currentUser.magicpassive+"%\n";
			col2 += "Projectile Damage: +"+currentUser.distancepassive+"%\n";
			col2 += "Life On Attack: "+currentUser.lifeattack+"\n";
			col2 += "Mana On Attack: "+currentUser.manaattack+"\n\n";

			var col3 = "Health Points: "+currentUser.maxhp+"\n";
			col3 += "Health Passive: "+currentUser.hppassive+"\n";
			col3 += "Mana Points: "+manaS+"\n";
			col3 += "Mana Passive: "+mppassive+"\n";
			col3 += "Life Regen: +"+currentUser.liferegen+"\n";
			col3 += "Mana Regen: +"+currentUser.manaregen+"\n";
			col3 += "Life On Block: "+currentUser.lifeblock+"\n";
			col3 += "Mana On Block: "+currentUser.manablock+"\n";

			var data = {
	  			col1: col1,
	  			col2: col2,
	  			col3: col3
	  		}

			socket.emit('PASSIVESTATS', data);
		}
	});

	socket.on('RESETPASSIVE', function(data){
		if(currentUser){				
			currentUser.maxlifecharges = 3;
			currentUser.liferegen = 1;
			currentUser.blockchance = 0;
			currentUser.evasionpassive = 0;
			currentUser.lifeblock = 0;
			currentUser.manablock = 0;
			currentUser.attackcd = 3;
			currentUser.manaregen = 1;
			currentUser.speed = 0;

			currentUser.meleepassive = 0;
			currentUser.magicpassive = 0;
			currentUser.distancepassive = 0;

			currentUser.hppassive = 0;
			currentUser.mppassive = 0;

			currentUser.armourpassive = 0;
			currentUser.criticalchancepassive = 0;
			currentUser.lifeattack = 0;
			currentUser.manaattack = 0;

			currentUser.passive = '0-';
			currentUser.points = parseInt(currentUser.level);

			currentUser.isDead = true;
		 	socket.broadcast.emit('USER_DISCONNECTED', currentUser);//emite para o metodo NetworkController.OnUserDisconnected(SocketIOEvent _myPlayer)

		 	/*if(currentUser.inparty){
		 		//console.log('was in party, remove from party');
		 		var wipeout = false;
				parties.forEach(function(party, index, object) {
					if(party.name == currentUser.charactername){
					  	////console.log('was the leader');
					  	for (var i = 0; i < clients.length; i++)
					 	{
							if (clients[i].charactername == party.p2 || clients[i].charactername == party.p3 || clients[i].charactername == party.p4 || clients[i].charactername == party.p5) 
							{
								clients[i].inparty = false;
							  	socket.broadcast.to(clients[i].socketid).emit('REMOVEPARTY', party);
							}
						}
					    object.splice(index, 1);
					} else if(party.p2 == currentUser.charactername){
						party.p2 = '';
						if(party.p3 == '' && party.p4 == '' && party.p5 == '')
							wipeout = true;
					} else if(party.p3 == currentUser.charactername){
						party.p3 = '';
						if(party.p2 == '' && party.p4 == '' && party.p5 == '')
							wipeout = true;
					} else if(party.p4 == currentUser.charactername){
						party.p4 = '';
						if(party.p3 == '' && party.p2 == '' && party.p5 == '')
							wipeout = true;
					} else if(party.p5 == currentUser.charactername){
						party.p5 = '';
						if(party.p3 == '' && party.p4 == '' && party.p2 == '')
							wipeout = true;
					}

					if(wipeout){
						wipeout = false;
						//console.log('wipe party out');
					  	for (var i = 0; i < clients.length; i++)
					 	{
							if (clients[i].charactername == party.name) 
							{
								clients[i].inparty = false;
							  	socket.broadcast.to(clients[i].socketid).emit('REMOVEPARTY', party);
							}
						}
					    object.splice(index, 1);
					}
				});
		 	}*/

		 	delete clients[currentUser.id];
			delete clientsOnline[currentUser.name];
		 
		 	console.log(clients);

			database_model.SavePlayer(currentUser, function (err, rows) {
			    if (err) { 
			    	console.error(err); 
			    }
		 	});
		}
	});

	// OTIMIZAR
	socket.on('COLLWITHPROJECTILE', function(data){
		if(currentUser){
			// player was off, another player is sending the damage for him
			if(currentUser.index != data.index){
				var otherUser = clients[data.index];

				if(otherUser == undefined){
					console.log("COLLWITHPROJECTILE não encontrei otherUser: "+data.index);
					return;
				}



				var blocked = false;
				var evaded = false;
				var shieldArmor = 0;

				if(otherUser.shield != 0){ // with shield
					shieldArmor = itemArmorById(otherUser.shield);
					var random = Math.floor((Math.random() * 100) + 1);
					if(random <= (otherUser.blockchance + 10)){
						blocked = true;
						if(otherUser.lifeblock > 0 && otherUser.hp < (otherUser.maxhp + otherUser.hppassive))
						{
							otherUser.hp = otherUser.hp + otherUser.lifeblock;
							if(otherUser.hp > (otherUser.maxhp + otherUser.hppassive))
								otherUser.hp = otherUser.maxhp + otherUser.hppassive;

							io.emit('LIFEBLOCK', otherUser);
						}

						io.emit('BLOCKED', otherUser);
					} 
				} else{ // don't have shield, let's see if he does have evasion
					var random = Math.floor((Math.random() * 100) + 1);
					if(random <= (otherUser.evasionpassive)){
						evaded = true;
						io.emit('EVADED', otherUser);
					}
				}

				if(!evaded){
					currentUser.criticized = false;

					if(data.criticized.includes("player")){ // monster
						if(!data.criticized.includes("spell")){
							index  = data.criticized.replace("player", '');
							data.damage = (currentUser.damage -  Math.round((otherUser.defense * 100) * (1 + (otherUser.armourpassive/100))) - clients[index].level)/10; // damage reduction
						}
						else{
							index  = data.criticized.replace("playerspell", '');
							data.damage = (currentUser.spelldamage -  Math.round((otherUser.defense * 100) * (1 + (otherUser.armourpassive/100))) - clients[index].level)/10;
						}
					}

					if(data.damage > 0){
						//console.log('dano apos penetrar defesa: '+data.damage);
						data.criticized = false;
					  	var damageVariation = Math.floor((Math.random() * 20) + 1);
						var plusOrMinus = Math.random() < 0.5 ? -1 : 1;
						data.damage = parseInt(data.damage) + (damageVariation * plusOrMinus * (data.damage / 100));
						//console.log('dano apos variacao: '+data.damage);
						var critical = Math.random() < (currentUser.criticalchancepassive/100) ? -1 : 1;

						if(critical == -1){
							data.criticized = true;
							data.damage *= 1.5;
							//console.log('dano apos critico: '+data.damage);
						}

						data.damage = Math.round(data.damage);
						
						////console.log('HP: '+currentUser.hp);
					  	if(currentUser.lifeattack > 0 && currentUser.hp < (currentUser.maxhp + currentUser.hppassive)){
					  		currentUser.hp = parseInt(currentUser.hp) + currentUser.lifeattack;
					  		////console.log('HP AFTER: '+currentUser.hp);
					  		if(currentUser.hp > (currentUser.maxhp + currentUser.hppassive))
					  			currentUser.hp = currentUser.maxhp + currentUser.hppassive;

					  		io.emit('ATTACKHEAL', currentUser);
					  	}
					}

					if(currentUser.pkTime > 0){
				  		currentUser.pkTime = 60;
			    		io.emit("RESETPKTIME", currentUser);
				  	}

				  	if(blocked)
						data.damage -= (shieldArmor*2)/100 * data.damage;

					if(data.damage > 0){
						data.damageonlife = 0;
						if(otherUser.manashield != 0 && otherUser.mp > 0){
							data.damageonlife = (1 - otherUser.manashield/100) * data.damage;
							data.damageonmana = data.damage - data.damageonlife;

							otherUser.mp = otherUser.mp - data.damageonmana;

							if(otherUser.mp < 0) { // took more damage then what he had of mana
								data.damageonlife = otherUser.mp * (-1);
								otherUser.mp = 0;
							}

							data.damage = data.damageonlife;
						}
						otherUser.hp = otherUser.hp - parseInt(data.damage);

						if(otherUser.hp <= 0){
							otherUser.x = 0;
							otherUser.y = 0;
							otherUser.hp = otherUser.maxhp + otherUser.hppassive;

							if(!otherUser.blessed){
								// make him loose exp and skill
								otherUser.xp = (otherUser.xp * 99)/100 ;

								var expToLevel = (50 * (((Math.pow(otherUser.level, 3) - (6 * (Math.pow(otherUser.level, 2)))) + (17 * otherUser.level)) - 12)) / 3;
								if(otherUser.xp < expToLevel)
									otherUser.xp = expToLevel;

								otherUser.meleep = (otherUser.meleep * 80)/100;
								otherUser.magicp = (otherUser.magicp * 80)/100;
								otherUser.distancep = (otherUser.distancep * 80)/100;

								otherUser.gold = (otherUser.gold * 95)/100;
								otherUser.gold = otherUser.gold + (otherUser.gold * 5)/100;
							} else
								otherUser.blessed = false;

							console.log('player dead, save');
							database_model.SavePlayer(otherUser, function (err, rows) {
							    if (err) { 
							    	console.error(err); 
							    }
						 	});
						}


						otherUser.lastdamage = parseInt(data.damage);

						io.emit('TAKEDAMAGE', otherUser);
					}
				}
			}
			else{
				var blocked = false;
				var evaded = false;
				var shieldArmor = 0;
				////console.log('random: '+random+' blockchance: '+currentUser.blockchance);
				if(currentUser.shield != 0){ // with shield
					shieldArmor = itemArmorById(currentUser.shield);
					var random = Math.floor((Math.random() * 100) + 1);
					if(random <= (currentUser.blockchance + 10)){
						blocked = true;
						if(currentUser.lifeblock > 0 && currentUser.hp < (currentUser.maxhp + currentUser.hppassive))
						{
							currentUser.hp = currentUser.hp + currentUser.lifeblock;
							if(currentUser.hp > (currentUser.maxhp + currentUser.hppassive))
								currentUser.hp = currentUser.maxhp + currentUser.hppassive;

							io.emit('LIFEBLOCK', currentUser);
						}
						if(currentUser.manablock > 0 && currentUser.mp < currentUser.maxmp)
						{
							currentUser.mp = currentUser.mp + currentUser.manablock;
							if(currentUser.mp > currentUser.maxmp)
								currentUser.mp = currentUser.maxmp;
						}

						io.emit('BLOCKED', currentUser);
					} 
				} else{ // don't have shield, let's see if he does have evasion
					var random = Math.floor((Math.random() * 100) + 1);
					if(random <= (currentUser.evasionpassive)){
						evaded = true;
						io.emit('EVADED', currentUser);
					}
				}

				if(!evaded){
					currentUser.criticized = false;
					if(!data.criticized.includes("player")){ // monster
						var monster = monsters[data.criticized];
						data.damage = monster.damage;

						data.damage = Math.floor((Math.random() * monster.damage) + 1);

						data.damage = data.damage -  Math.round((currentUser.defense * 5) * (1 + (currentUser.armourpassive/100))) - currentUser.level/2; // damage reduction
						
						if(data.damage >= monster.damage/1.5){ // making the monster crit only on high damages
							critical = Math.floor((Math.random() * 100) + 1);
							if(critical <= 8){
								data.criticized = 'true';
								currentUser.criticized = true;
								data.damage *= 1.5;
							}
						}
					} else{ // player
						var index;

						if(!data.criticized.includes("spell")){
							index  = data.criticized.replace("player", '');
							data.damage = (clients[index].damage -  Math.round((currentUser.defense * 100) * (1 + (currentUser.armourpassive/100))) - clients[index].level)/10; // damage reduction
						}
						else{
							index  = data.criticized.replace("playerspell", '');
							data.damage = (clients[index].spelldamage -  Math.round((currentUser.defense * 100) * (1 + (currentUser.armourpassive/100))) - clients[index].level)/10;
						}

						if(data.damage > 0){
							//console.log('dano apos penetrar defesa: '+data.damage);
							data.criticized = false;
						  	var damageVariation = Math.floor((Math.random() * 20) + 1);
							var plusOrMinus = Math.random() < 0.5 ? -1 : 1;
							data.damage = parseInt(data.damage) + (damageVariation * plusOrMinus * (data.damage / 100));
							//console.log('dano apos variacao: '+data.damage);
							var critical = Math.random() < (clients[index].criticalchancepassive/100) ? -1 : 1;

							if(critical == -1){
								data.criticized = true;
								data.damage *= 1.5;
								//console.log('dano apos critico: '+data.damage);
							}

							data.damage = Math.round(data.damage);
							
							////console.log('HP: '+clients[index].hp);
						  	if(clients[index].lifeattack > 0 && clients[index].hp < (clients[index].maxhp + clients[index].hppassive)){
						  		clients[index].hp = parseInt(clients[index].hp) + clients[index].lifeattack;
						  		////console.log('HP AFTER: '+clients[index].hp);
						  		if(clients[index].hp > (clients[index].maxhp + clients[index].hppassive))
						  			clients[index].hp = clients[index].maxhp + clients[index].hppassive;

						  		io.emit('ATTACKHEAL', clients[index]);
						  	}
						}

						if(data.damage >= 2) {
							if(clients[index].weaponType == 'melee'){
								clients[index].meleep++;

								var nextSkill = clients[index].melee + 1;
								var attacksToSkll = 50 * Math.pow(1.1, (clients[index].melee));

								////console.log('current skill: '+currentUser.melee);
								////console.log('attacks count: '+currentUser.meleep);
								////console.log('total attacks at: '+attacksToSkll);

								if(clients[index].meleep >= attacksToSkll){
									////console.log('advanced skill to '+nextSkill);
									clients[index].melee = nextSkill;
									clients[index].meleep = 0;
								}
							} else if(clients[index].weaponType == 'magic'){
								clients[index].magicp++;

								var nextSkill = clients[index].magic + 1;
								var attacksToSkll = 50 * Math.pow(1.1, (clients[index].magic));

								////console.log('current skill: '+currentUser.magic);
								////console.log('attacks count: '+clients[index].magicp);
								////console.log('total attacks at: '+attacksToSkll);

								if(clients[index].magicp >= attacksToSkll){
									////console.log('advanced skill to '+nextSkill);
									clients[index].magic = nextSkill;
									clients[index].magicp = 0;
								}
							} else if(clients[index].weaponType == 'distance'){
								clients[index].distancep++;

								var nextSkill = clients[index].distance + 1;
								var attacksToSkll = 50 * Math.pow(1.1, (clients[index].distance));

								////console.log('current skill: '+clients[index].distance);
								////console.log('attacks count: '+clients[index].distancep);
								////console.log('total attacks at: '+attacksToSkll);

								if(clients[index].distancep >= attacksToSkll){
									////console.log('advanced skill to '+nextSkill);
									clients[index].distance = nextSkill;
									clients[index].distancep = 0;
								}
							}
						}

					  	if(clients[index].pkTime > 0){
					  		////console.log(clients[index].charactername+" estava pk e atacou, resetar tempo de pk");
					  		clients[index].pkTime = 60;
				    		io.emit("RESETPKTIME", clients[index]);
					  	}
					}

					if(blocked)
						data.damage -= (shieldArmor*2)/100 * data.damage;

					if(data.damage > 0){
						data.damageonlife = 0;
						if(currentUser.manashield != 0 && currentUser.mp > 0){
							//console.log('has mana shield: '+currentUser.manashield+'%');

							data.damageonlife = (1 - currentUser.manashield/100) * data.damage;
							data.damageonmana = data.damage - data.damageonlife;

							//console.log('damage on mana: '+data.damageonmana);

							currentUser.mp = currentUser.mp - data.damageonmana;

							if(currentUser.mp < 0) { // took more damage then what he had of mana
								data.damageonlife = currentUser.mp * (-1);
								currentUser.mp = 0;
							}
							
							data.damage = data.damageonlife;
						}

						//console.log('final damage on life: '+data.damage);

						currentUser.hp = currentUser.hp - parseInt(data.damage);

						if(currentUser.hp <= 0){
							////console.log(currentUser.charactername+' is dead!!');
							currentUser.x = 0;
							currentUser.y = 0;
							currentUser.hp = 0;
						}

						currentUser.lastdamage = parseInt(data.damage);
						currentUser.mp = parseInt(currentUser.mp);

						io.emit('TAKEDAMAGE', currentUser);
					}
				}
			}
		}
	});

	socket.on('PARTY', function(data){
		if(currentUser){
			if(!currentUser.inparty){
				//console.log('sender not in party, verify if other player is in party');
				if(clients[data.index] != undefined){
					if(clients[data.index].inparty != false)
						otherplayerinparty = true;
				  	else{
				  		//console.log('sender and other player not in party, send invitation');
						socket.broadcast.to(clients[data.index].socketid).emit('PARTYINVITATION', currentUser);
				  	}
				  }
			} else{
				//console.log('sender in party, verify if is the leader');
				var party = parties[currentUser.party];
				if(party != undefined){
					if(party.p1id == currentUser.index){
						if(party.p3 == '' || party.p4 == '' || party.p5 == ''){
			    			//console.log('party has free slot, send invitation');
							socket.broadcast.to(clients[data.index].socketid).emit('PARTYINVITATION', currentUser);
			    		}
					}
				}
			}
		}
	});

	socket.on('TRADE', function(data){
		if(currentUser){
			socket.broadcast.to(clients[data.index].socketid).emit('TRADEREQUEST', currentUser);
		}
	});

	socket.on('ACCEPTPARTY', function(data){
		if(currentUser){ // quem aceitou a party
			if(clients[data.index].inparty){
				var party = parties[clients[data.index].party];

				if(party == undefined){
					console.log("acceptparty undefined");
					return;
				}

				////console.log('found party, get the free slot');
				if(party.p3 == ''){
					////console.log('p3 is free');
					party.p3 = currentUser.charactername;
					party.p3id = currentUser.index;
				} else if(party.p4 == ''){
					party.p4 = currentUser.charactername;
					party.p4id = currentUser.index;
					////console.log('p4 is free');
				} else if(party.p5 == ''){
					////console.log('p5 is free');
					party.p5 = currentUser.charactername;
					party.p5id = currentUser.index;
				}

				currentUser.inparty = true;
				currentUser.party = clients[data.index].party;

				socket.broadcast.to(clients[party.p1id].socketid).emit('ACCEPTPARTY', party);

				if(party.p2 != '')
					socket.broadcast.to(clients[party.p2id].socketid).emit('ACCEPTPARTY', party);
				if(party.p3 != '')
					socket.broadcast.to(clients[party.p3id].socketid).emit('ACCEPTPARTY', party);
				if(party.p4 != '')
					socket.broadcast.to(clients[party.p4id].socketid).emit('ACCEPTPARTY', party);
				if(party.p5 != '')
					socket.broadcast.to(clients[party.p5id].socketid).emit('ACCEPTPARTY', party);

				socket.emit('ACCEPTPARTY', party);
				return;
			}

			//console.log('create party');
			var party = {
	  			name: data.charactername,
	  			p1: data.charactername,
	  			p2: currentUser.charactername,
	  			p3: '',
	  			p4: '',
	  			p5: '',
	  			p1id: parseInt(data.index),
	  			p2id: currentUser.index,
	  			p3id: '',
	  			p4id: '',
	  			p5id: ''
	  		}
	  		parties.push(party);
	  		////console.log(parties);

	  		currentUser.inparty = true;
	  		currentUser.party = parties.length - 1;

	  		clients[party.p1id].inparty = true;
	  		clients[party.p1id].party = parties.length - 1;

		  	socket.broadcast.to(clients[party.p1id].socketid).emit('ACCEPTPARTY', party);
			socket.emit('ACCEPTPARTY', party);			
		}
	});

	socket.on('ACCEPTTRADE', function(data){
		if(currentUser){
			currentUser.traderIndex = data.index;
			if(clients[data.index]){
				clients[data.index].traderIndex = currentUser.index;

				socket.emit('ACCEPTTRADE');
				socket.broadcast.to(clients[data.index].socketid).emit('ACCEPTTRADE');
			}
		}
	});

	socket.on('CANCELTRADE', function(data){
		if(currentUser){
			currentUser.goldToTrade = 0;
			if(clients[data.index] != undefined)
				clients[data.index].goldToTrade = 0;

			socket.emit('CANCELTRADE');
			if(clients[currentUser.traderIndex] != undefined)
				socket.broadcast.to(clients[data.index].socketid).emit('CANCELTRADE');
		}
	});

	socket.on('ITEMTOTRADE', function(data){
		if(clients[currentUser.traderIndex] != undefined)
			socket.broadcast.to(clients[currentUser.traderIndex].socketid).emit('ITEMTOTRADE', data);
	});

	socket.on('GOLDTRADE', function(data){
		currentUser.goldToTrade = data.gold;

		if(clients[currentUser.traderIndex] != undefined)
			socket.broadcast.to(clients[currentUser.traderIndex].socketid).emit('GOLDTRADE', data);
	});

	socket.on('UNSELECTITEMTOTRADE', function(data){
		if(clients[currentUser.traderIndex] != undefined)
			socket.broadcast.to(clients[currentUser.traderIndex].socketid).emit('UNSELECTITEMTOTRADE', data);
	});

	socket.on('TRADEBLOCKER', function(data){
		currentUser.acceptedTrade = true;
		currentUser.item1trade = data.item1id;
		currentUser.item2trade = data.item2id;
		currentUser.item3trade = data.item3id;
		currentUser.item4trade = data.item4id;
		currentUser.item5trade = data.item5id;

		currentUser.slot1 = parseInt(data.slot1);
		currentUser.slot2 = parseInt(data.slot2);
		currentUser.slot3 = parseInt(data.slot3);
		currentUser.slot4 = parseInt(data.slot4);
		currentUser.slot5 = parseInt(data.slot5);
		currentUser.slot6 = parseInt(data.slot6);
		currentUser.slot7 = parseInt(data.slot7);
		currentUser.slot8 = parseInt(data.slot8);
		currentUser.slot9 = parseInt(data.slot9);

		if(clients[currentUser.traderIndex] != undefined){
			var client = clients[currentUser.traderIndex];
			if(client.acceptedTrade == true){
				//console.log('other player already accepted the trade, make the trade. Last to accept: '+currentUser.charactername);

				currentUser.gold -= parseInt(currentUser.goldToTrade);
				currentUser.gold += parseInt(client.goldToTrade);
				client.gold -= parseInt(client.goldToTrade);
				client.gold += parseInt(currentUser.goldToTrade);

				currentUser.goldToTrade = 0;
				client.goldToTrade = 0;

				currentUser.acceptedTrade = false;
				client.acceptedTrade = false;

				for(var j = 1; j <= 5; j++){
					//console.log('other item id to trade: '+client['item'+j+'trade']);
					if(client['item'+j+'trade'] != 0 && client['item'+j+'trade'] != undefined){
						for(var i = 1; i <= 9; i++){
							if(currentUser['slot'+i] == 0){
								currentUser['slot'+i] = parseInt(client['item'+j+'trade']);
								break;
							}
						}
					}

					//console.log('current item id to trade: '+currentUser['item'+j+'trade']);
					if(currentUser['item'+j+'trade'] != 0 && currentUser['item'+j+'trade'] != undefined){
						for(var i = 1; i <= 9; i++){
							if(client['slot'+i] == 0){
								client['slot'+i] = parseInt(currentUser['item'+j+'trade']);
								break;
							}
						}
					}
				}

				socket.emit('FINISHTRADE', currentUser);
				socket.broadcast.to(client.socketid).emit('FINISHTRADE', client);
			} else
				socket.broadcast.to(clients[currentUser.traderIndex].socketid).emit('TRADEBLOCKER');
		}
	});

	// OTIMIZAR
	socket.on('FINISHEDREWARDEDVIDEO', function(data){
		adscount++;
		if(adscount >= 100){
			//console.log('============== 1K VIDEOS ==============');
			adscount = 0;
			serverexp = 1.5;
			setTimeout(expFunc, 3600000, 1); //3600000 // 1 hour
		}
		io.emit('ADSCOUNT', {count:adscount});
	});

	// OTIMIZAR
	socket.on('BUYBLESS', function(data){
		if(currentUser){
			var blessCost = ((50 * (((Math.pow(currentUser.level, 3) - (6 * (Math.pow(currentUser.level, 2)))) + (17 * currentUser.level)) - 12)) / 3) * 0.02;
			if(blessCost < 10000)
				blessCost = 10000;

			if(currentUser.gold >= blessCost && !currentUser.blessed){
				currentUser.gold -= blessCost;
				currentUser.blessed = true;
				socket.emit('BUYBLESS', currentUser);
			}
		}
	});

	socket.on('LOTTERYGOLD', function(data){
		var data = {
  			lotteryGold: lotteryGold
  		}
		socket.emit('LOTTERYGOLD', data);
	});

	socket.on('LOTTERYLOOSE', function(data){
		if(currentUser){
			currentUser.gold -= Math.floor(lotteryGold * 0.1);
			lotteryGold += Math.floor(lotteryGold * 0.1);
		}
	});

	socket.on('LOTTERYWIN', function(data){
		if(currentUser){	
			console.log(currentUser.charactername+' won the lottery');
			currentUser.gold += lotteryGold;
			lotteryGold = 0;
		}
	});
	
	socket.on('PURCHASED25DARKOINS', function(data){
		if(currentUser){
			console.log(currentUser.charactername+' bought 25  darkoins');
			currentUser.darkoins += 25;

			database_model.SaveDarkoins(currentUser.id, 25, data.paymentid, data.currency, data.price, function (err, rows) {
			    if (err) { 
			    	console.error(err); 
			    }
			});
		}
	});

	socket.on('PURCHASED50DARKOINS', function(data){
		if(currentUser){
			console.log(currentUser.charactername+' bought 50  darkoins');
			currentUser.darkoins += 50;

			database_model.SaveDarkoins(currentUser.id, 50, data.paymentid, data.currency, data.price, function (err, rows) {
			    if (err) { 
			    	console.error(err); 
			    }
			});
		}
	});

	socket.on('PURCHASED100DARKOINS', function(data){
		if(currentUser){
			console.log(currentUser.charactername+' bought 100  darkoins');
			currentUser.darkoins += 100;

			database_model.SaveDarkoins(currentUser.id, 100, data.paymentid, data.currency, data.price, function (err, rows) {
			    if (err) { 
			    	console.error(err); 
			    }
			});
		}
	});










    // OTIMIZAR
	socket.on('disconnect', function () {
        console.log("User has disconnected");

	    if(currentUser)
		{
		 	currentUser.isDead = true;
		 	console.log(currentUser.charactername+' disconnected');

		 	var d = new Date();
			var n = d.getTime();
			clientsOnline[currentUser.name] = n;
		 	/*console.log('pk ? deve deslogar ou não? '+currentUser.pkTime);
		 	if(currentUser.pkTime == null || currentUser.pkTime == 0){
		 		console.log('deslogar');
		 		socket.broadcast.emit('USER_DISCONNECTED', currentUser);//emite para o metodo NetworkController.OnUserDisconnected(SocketIOEvent _myPlayer)
		 	} else{
		 		console.log('não deslogar, deslogar após 30 segundos');
		 		setTimeout(function() {
					socket.broadcast.emit('USER_DISCONNECTED', currentUser);
				}, 10000);
		 	}*/

			socket.broadcast.emit('USER_DISCONNECTED', currentUser);

			if(clients[currentUser.traderIndex] != undefined)
				socket.broadcast.to(clients[currentUser.traderIndex].socketid).emit('CANCELTRADE');

		 	setTimeout(function(){ 
				//console.log(clients[currentUser.id]);
				if(clients[currentUser.id]){
			 		if(clients[currentUser.id].isDead){
						console.log("REMOVING PLAYER: "+currentUser.charactername);
						socket.broadcast.emit('USER_DESTROY', currentUser);
						delete clients[currentUser.id];
						delete clientsOnline[currentUser.name];
					}
				}
		 	}, 30000);

			database_model.SavePlayer(currentUser, function (err, rows) {
			    if (err) { 
			    	console.error(err); 
			    }
		 	});

		 	if(currentUser.inparty){
		 		//console.log(currentUser.charactername+' was in party, remove from party');
		 		var wipeout = false;

		 		var party = parties[currentUser.party];

		 		//console.log(party);

		 		if(party == undefined)
		 			return;

				if(party.p1 == currentUser.charactername){
					//console.log('was the leader, wipeout party');
					if(party.p2 != ''){
						if(clients[party.p2id] != undefined){
							clients[party.p2id].inparty = false;
							socket.broadcast.to(clients[party.p2id].socketid).emit('REMOVEPARTY', party);
						}
					}
					if(party.p3 != ''){
						if(clients[party.p3id] != undefined){
							clients[party.p3id].inparty = false;
							socket.broadcast.to(clients[party.p3id].socketid).emit('REMOVEPARTY', party);
						}
					}
					if(party.p4 != ''){
						if(clients[party.p4id] != undefined){
							clients[party.p4id].inparty = false;
							socket.broadcast.to(clients[party.p4id].socketid).emit('REMOVEPARTY', party);
						}
					}
					if(party.p5 != ''){
						if(clients[party.p5id] != undefined){
							clients[party.p5id].inparty = false;
							socket.broadcast.to(clients[party.p5id].socketid).emit('REMOVEPARTY', party);
						}
					}

					parties.splice(currentUser.party, 1);
				} else{
					//console.log('not the leader');
					io.emit('REMOVEPLAYERPARTY', currentUser);
					if(party.p2 == currentUser.charactername){
						party.p2 = '';
						party.p2id = '';
					} else if(party.p3 == currentUser.charactername){
						party.p3 = '';
						party.p3id = '';
					} else if(party.p4 == currentUser.charactername){
						party.p4 = '';
						party.p4id = '';
					} else if(party.p5 == currentUser.charactername){
						party.p5 = '';
						party.p5id = '';
					}

					var countPlayerParty = 0;

					if(party.p1 != '')
						countPlayerParty++;
					if(party.p2 != '')
						countPlayerParty++;
					if(party.p3 != '')
						countPlayerParty++;
					if(party.p4 != '')
						countPlayerParty++;
					if(party.p5 != '')
						countPlayerParty++;

					//console.log('sobrou '+countPlayerParty+' player na party');

					if(countPlayerParty <= 1){
						//console.log('remover party da lista');
						if(clients[party.p1id]){
							socket.broadcast.to(clients[party.p1id].socketid).emit('REMOVEPARTY', party);
							clients[party.p1id].inparty = false;
						}

						parties.splice(currentUser.party, 1);
					}
				}
		 	}
		} else
		{
			//console.log('no currentUser');
		}
    });//END-SOCKET.ON

	/***********************************************************************************************************************************/
	
		
});//END-IO.ON
/***********************************************************************************************************************************/

var port  = 3000;
http.listen(process.env.PORT || port, function(){
	console.log('listening on *:'+port);
});

//console.log("------- server is running -------");

database_model.get_monsters(function (err, rows) {
	if (err) { 
		console.error(err); 
	}
	else{
		// rows[0] = monsters
		// rows[1] = items
		// rows[1][0] = first loot (id = 1)

		rows[1].forEach(function(loot){
			loots[loot.id] = loot;
	  	});

		rows[0].forEach(function(monster){
	    	monster.target = null;
	    	monster.isDead = false;
	    	monster.name = monster.name + monster.id;
	    	monster.startX = monster.x;
	    	monster.startY = monster.y;
	    	monster.currentX = monster.x;
	    	monster.currentY = monster.y;
	    	monster.loot = monster.loot;
	    	monster.index = monsters.length;
	    	monsters.push(monster);
	   		//console.log(monster);
	  	});

		////console.log(loots);
		//console.log(monsters);
		//console.log("done loading monsters");
		setInterval(saveFunc, 300000); //600000
	}
});

database_model.itensList(function (err, rows) {
	if (err) { 
		console.error(err); 
	}
	else{
		rows.forEach(function(item){
	    	itens[item.id] = item;
	    	itensName[item.name] = item;
	  	});
	}
});

database_model.lotteryGold(function (err, rows) {
	if (err) { 
		console.error(err); 
	}
	else{
		rows.forEach(function(gold){
	    	lotteryGold = gold.gold;
	  	});

		console.log('gold on lottery: '+lotteryGold);
	}
});

// function intervalFunc() {
// 	if(clients.length > 0){
// 		if(done){
// 			done = false;

//   			monsters.forEach(function(monster){
//   				if(monster.isDead == false){
// 		    		io.emit("UPDATEMONSTERS", monster);
//   				}
// 		  	});

// 		  	done = true;
// 		}
// 	}
// }

function myFunc(arg) {
	// console.log(`arg was => ${arg}`);
  	monsters[arg].x = monsters[arg].startX;
	monsters[arg].y = monsters[arg].startY;
	monsters[arg].hp = monsters[arg].maxhp;
	// console.log("emit spawn");
	// console.log(monsters[arg]);
	io.emit("REVIVEMONSTER", monsters[arg]);
	monsters[arg].isDead = false;
}

function expFunc(arg) {
	////console.log(`arg was => ${arg}`);
	serverexp = 1;
}

function saveFunc() {
	database_model.SaveLotteryGold(lotteryGold, function (err, rows) {
	    if (err) { 
	    	console.error(err); 
	    }
	});

	for (var client in clients) {
        database_model.SavePlayer(clients[client], function (err, rows) {
		    if (err) { 
		    	console.error(err); 
		    }
 		});
	}
}

function itemArmorById(id) {
	return itens[id].armor;
}

function itemDamageById(id) {
	return itens[id].damage;
}

function damageCalculation(currentUser){
	var damage;

	if(itens[currentUser.weapon].type == "magic")
		damage = Math.round(0.8 * itemDamageById(currentUser.weapon) * (currentUser.magic * (1 + currentUser.magicpassive/100))) * (1 + currentUser.level / 1000);
	else if(itens[currentUser.weapon].type == "distance")
		damage = Math.round(0.95 * itemDamageById(currentUser.weapon) * (currentUser.distance * (1 + currentUser.distancepassive/100))) * (1 + currentUser.level / 1000);
	else if(itens[currentUser.weapon].type == "melee")
		damage = Math.round(0.93 * itemDamageById(currentUser.weapon) * (currentUser.melee * (1 + currentUser.meleepassive/100))) * (1 + currentUser.level / 1000);
	else
		damage = currentUser.level;

	//console.log("damage: "+damage);

	return parseInt(damage);
}

function spellDamageCalculation(currentUser) {
	var spellDamage = 0;

	if(currentUser.skill == "Explosion" && currentUser.weaponType == "magic")
		spellDamage = Math.round(1 * itemDamageById(currentUser.weapon) * (currentUser.magic * (1 + currentUser.magicpassive/70))) * (1 + currentUser.level / 1000);
	else if(currentUser.skill == "Blast" && currentUser.weaponType == "distance")
		spellDamage = currentUser.level + ((currentUser.distance + currentUser.distancepassive) * 1);
	else if(currentUser.skill == "Sweep" && currentUser.weaponType == "melee")
		spellDamage = Math.round(1.2 * itemDamageById(currentUser.weapon) * (currentUser.melee * (1 + currentUser.meleepassive/100))) * (1 + currentUser.level / 1000);
	else if(currentUser.skill == "PowerfulPrecision" && currentUser.weaponType == "distance")
		spellDamage = Math.round(1.4 * itemDamageById(currentUser.weapon) * (currentUser.distance * (1 + currentUser.distancepassive/85))) * (1 + currentUser.level / 1000);

	//console.log("spell damage: "+spellDamage);

	return parseInt(spellDamage);
}

// setInterval(() => io.emit('time', new Date().toTimeString()), 1000);